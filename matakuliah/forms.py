from django import forms

from .models import Matkul

class InputForm(forms.ModelForm):
    class Meta :
        model = Matkul
        fields = [
            'Mata_Kuliah', 'Dosen_Pengajar', 'SKS', 'kelas','deskripsi',
        ]

        widgets = {
            'Mata_Kuliah' : forms.TextInput(attrs={'class': 'form-control'}),
            'Dosen_Pengajar' : forms.TextInput(attrs={'class': 'form-control'}),
            'SKS' : forms.TextInput(attrs={'class': 'form-control'}),
            'kelas' : forms.TextInput(attrs={'class': 'form-control'}),
            'deskripsi' : forms.Textarea(attrs={'class': 'form-control'}),
        }
