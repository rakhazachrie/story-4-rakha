from django.db import models

# Create your models here.
class Matkul (models.Model):
    Mata_Kuliah = models.CharField(max_length = 30, default=" ")
    Dosen_Pengajar = models.CharField(max_length = 25, default=" ")
    SKS = models.DecimalField(max_digits = 1, decimal_places=0, default=3)
    deskripsi = models.TextField()
    kelas = models.CharField(max_length = 25, default=" ")