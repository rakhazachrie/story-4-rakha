from django.test import TestCase, Client

# Create your tests here.
class TestMatkul :
    def test_url_matkul_exist(self) :
        response = Client().get('/story5/')
        self.assertEquals(response.status_code, 200)
    
    def test_url_add(self) :
        response = Client().get('/story5/add/')
        self.assertEquals(response.status_code, 200)

    def test_model_exist(self) :
        Matkul.objects.create(Mata_Kuliah="matkul", Dosen_Pengajar="dosen", SKS="3", deskripsi="abc", kelas="a")
        count_matkul = Matkul.objects.all().count()
        self.assertEquals(count_matkul, 1)
