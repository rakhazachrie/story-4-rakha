from django.urls import path

from . import views

app_name = 'matakuliah'

urlpatterns = [
    path('', views.matkul_view, name='matkul'),
    path('add/', views.form_view, name='add'),
    path('detail/<int:id>', views.detail_view, name='detail'),

]