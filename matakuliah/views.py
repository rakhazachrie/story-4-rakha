from django.shortcuts import render, redirect
from django.http import HttpResponse
from .forms import InputForm
from .models import Matkul
# Create your views here.

def form_view(request) :
    form = InputForm()
    if (request.method == 'POST') :
        form = InputForm(request.POST or None)
        if (form.is_valid):
            form.save()
            return redirect('matakuliah:matkul')
    context = {'input_form' : form}

    return render(request, 'matakuliah/form.html', context)

def matkul_view(request):
    matkuls = Matkul.objects.all()
    context = {}
    context['matkuls'] = matkuls

    if request.method == 'POST' :
        if request.POST.get("delete") :
            Matkul.objects.get(id=request.POST.get("delete")).delete()
            return redirect('matakuliah:matkul')

        elif request.POST.get("add") :
            return redirect('matakuliah:add')
        
        elif request.POST.get("detail"):
            Matkul.objects.get(id=request.POST.get("detail"))
            return redirect('matakuliah:detail')
    
    return render(request, "matakuliah/index.html", context)

def detail_view(request,id):
    matkul = Matkul.objects.get(id=id)
    context = {}
    context['matkul'] = matkul

    return render(request, "matakuliah/desc.html", context)


    