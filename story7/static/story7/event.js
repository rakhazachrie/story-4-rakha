$(document).ready(function() {
    $(".accordion .accord-header").click(function() {
      // for active header definition
      $('.accord-header').removeClass('on');
      $(this).addClass('on');
      
      // accordion actions
      if($(this).next("div").is(":visible")){
        $(this).next("div").slideUp(400);
        $(this).removeClass('on');
      } else {
        $(".accordion .accord-content").slideUp(400);
        $(this).next("div").slideToggle(400);
      }
   });
});


$(".up").click(function() {
    var accord = $(this).parents('.accord-part'),
        swapWith = accord.prev();
    accord.after(swapWith);
});

$(".down").click(function() {
    var accord = $(this).parents('.accord-part'),
        swapWith = accord.next();
    accord.before(swapWith);
});
