from django.test import TestCase, Client
from http import HTTPStatus
from django.urls import resolve, reverse
from . import views

# Create your tests here.
class TestStory7(TestCase) :
    def test_url_story7_200(self) :
        response = Client().get('/story7/')
        self.assertEquals(response.status_code, 200)

    def test_story7_template(self) :
        response = Client().get('/story7/')
        self.assertTemplateUsed(response, 'story7/index.html')
    
    def test_story7_views(self) :
        function = resolve('/story7/')
        self.assertEqual(function.func, views.story7)

