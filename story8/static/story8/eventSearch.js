$(".keyword").keyup( function() {
    var word = $(".keyword").val();
    $.ajax({
        url: '/story8/data?q=' + word,
        success: function(data) {
            var list_books = data.items;
            $(".list-books").empty();
            $(".list-books").append("<tr><th>Title</th> <th>Cover</th></tr>");
            for(i = 0; i < list_books.length; i++) {
                var bookTitle = list_books[i].volumeInfo.title;
                var bookPict = list_books[i].volumeInfo.imageLinks.smallThumbnail;
                $(".list-books").append("<tr><td>" + bookTitle + "</td><td><img src=" + bookPict + "/td></tr>");
            }
        }
    });
});