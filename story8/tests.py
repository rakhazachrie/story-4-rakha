from django.test import TestCase, Client
from django.urls import resolve, reverse
from . import views

# Create your tests here.
class TestStory8(TestCase) :
    def test_story8_200(self) :
        response = Client().get('/story8/')
        self.assertEquals(response.status_code, 200)

    def test_story8_template(self) :
        response = Client().get('/story8/')
        self.assertTemplateUsed(response, 'story8/index.html')

    def test_story8_views(self) :
        function = resolve('/story8/')
        self.assertEqual(function.func, views.story8)

    # def test_json_response(self) :
    #     response = self.client.post('/data/')
    #     self.assertJSONEqual(str(response.content,encoding='utf-8'), )
