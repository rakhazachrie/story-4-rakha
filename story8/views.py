from django.shortcuts import render
from django.http import JsonResponse
import requests
import json

# Create your views here.
def story8(request) :
    return render(request, 'story8/index.html')

def fungsi_data_story8(request) :
    url = "https://www.googleapis.com/books/v1/volumes?q=" + request.GET['q']
    urlReturn = requests.get(url)
    data = json.loads(urlReturn.content)
    return JsonResponse(data, safe=False)
