from django.test import TestCase, Client
from django.contrib.auth import get_user_model, authenticate
from django.contrib.auth.models import User

# Create your tests here.
class TestStory9(TestCase) :
    def test_story9_200(self) :
        response = Client().get('/story9/')
        self.assertEquals(response.status_code, 200)

    def test_story9_template(self) :
        response = Client().get('/story9/')
        self.assertTemplateUsed(response, 'story9/login.html')
    
    def setUp(self) :
        self.user = User.objects.create_user(username='test', password='12test12', email='test@example.com', is_staff=True)
        self.user.save()

    def test_user_login_success(self) :
        user = authenticate(username='test', password='12test12')
        self.assertTrue((user is not None) and user.is_authenticated)

    def test_logged_in(self) :
        user = User.objects.create(username='testuser', is_staff=True)
        user.set_password('12345')
        user.save()
        c = Client()
        logged_in = c.login(username='testuser', password='12345')
        self.assertTrue(logged_in) 


