from django.shortcuts import render, redirect, HttpResponseRedirect
from django.contrib.auth.forms import UserCreationForm
from .forms import CreateUserForm
from django.contrib.auth import authenticate, login, logout

# Create your views here.
def user_login(request) :
    if request.method == 'POST' :
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(request, username=username, password=password)

        if user is not None :
            login(request, user)
            return redirect('main:main')

    context = {}
    return render(request, 'story9/login.html', context)

def user_register(request) :
    if request.method == 'POST' :
        form = CreateUserForm(request.POST)
        if form.is_valid() :
            form.save()
            return redirect('story9:login')
    else :
        form = CreateUserForm()

    context = {'form':form}
    return render(request, 'story9/register.html', context)

def user_logout(request) :
    logout(request)
    return redirect('main:main')
